// liste des cases déjà touchées
var plateau = [];

// nombre de tentatives
var tentatives = 0;

// position du bateau
var bateau;

// elements de la page dont on va avoir besoin
var jouer = document.getElementById("jouer");
var colonne = document.getElementById("choixColonne");
var ligne = document.getElementById("choixLigne");
var divMessage = document.getElementById("message");

// messages
var message = [];
message[0] = "Plouf, dans l'eau !";
message[1] = "Bateau touché/coulé !";
message[2] = "Case déjà jouée !";

// fonction pour vérifier si une case a déjà été jouée
function laCaseNaPasEteJouee(position) {
    for (i = 0; i < plateau.length; i++) {
        if (plateau[i][0] == position[0] && plateau[i][1] == position[1]) {
            return false;
        }
    }
    return true;
}

// ce qui se passe quand on clique sur le bouton pour tirer
jouer.addEventListener('click', function () {
    var lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    // coordonées de la case qu'on attaque
    var coordonnees = [lettres.indexOf(colonne.value), ligne.value - 1];

    // on augmente de 1 le nombre de tentatives
    tentatives++;

    // est-ce que la case a déjà été jouée ?
    if (laCaseNaPasEteJouee(coordonnees)) {
        if (coordonnees[0] == bateau[0] && coordonnees[1] == bateau[1]) {
            // cas ou on gagne
            divMessage.innerHTML = message[1] + " (en " + tentatives + " tentative" + (tentatives > 1 ? "s" : "") + ")";
            document.getElementById(colonne.value + ligne.value).innerHTML = '<=>';
            // on affiche le bouton pour rejouer
            document.getElementById('rejouer').style.display = '';
        } else {
            // cas ou on tombe à l'eau
            divMessage.innerHTML = message[0];
            document.getElementById(colonne.value + ligne.value).innerHTML = '~';
        }
        plateau.push(coordonnees);
    } else {
        divMessage.innerHTML = message[2];
    }

});

// recommencer ou commencer une partie
function rejouer() {
    // nouvelles coordonnées pour le bateau
    bateau = [Math.floor(Math.random() * 4), Math.floor(Math.random() * 4)];

    // on remet à 0 les cases déjà jouées et le nombre de tentatives
    plateau = [];
    tentatives = 0;

    // on remet toutes les cases à 0 avec un "?"
    var cases = document.getElementsByClassName('caseBataille');
    for (i = 0; i < cases.length; i++) {
        cases[i].innerHTML = '?';
    }

    // on vide les input
    colonne.value = '';
    ligne.value = '';

    // on masque le bouton pour rejouer
    document.getElementById('rejouer').style.display = 'none';

    // on vide le résultat d'une attaque
    divMessage.innerHTML = '??????';

}

// on lance la première partie
rejouer();

// initialisation du bouton pour rejouer
document.getElementById('rejouer').addEventListener('click', rejouer);
